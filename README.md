# Nametree

Nametree is an online application to create diagrams for the names of your loved ones, non-human and human. It is designed to appreciate the diversity of names and their connections.

Because nobody has just one name.

Demo diagram photo: [localpups on Flickr](https://www.flickr.com/photos/133374862@N02/).