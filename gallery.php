<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="fonts/stylesheet.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="shortcut icon" href="img/favicon.svg" type="image/x-icon">
  <title>Nametree Gallery</title>
</head>
<body class="gallery">
  <header>
    <a class="button gallery-back" href="./">Back to app</a>
  </header>
  <main>
  <?php
    $imagesDir = 'trees/';
    $images = glob($imagesDir . '*.{png}', GLOB_BRACE);
    function extractPublicationDate($filename) {
      $filenameParts = explode('-', $filename);
      $dateString = end($filenameParts);
      return DateTime::createFromFormat('YmdHis', substr($dateString, 0, 14));
    }
    usort($images, function($a, $b) {
      return extractPublicationDate($b) <=> extractPublicationDate($a);
    });
    foreach ($images as $image) {
      echo '<img src="' . $image . '" alt="Tree Image"><br>';
    }
  ?>  
  </main>
</body>
</html>
