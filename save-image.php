<?php
if (isset($_POST['imageData']) && isset($_POST['name'])) {
  $data = $_POST['imageData'];
  $name = slugify($_POST['name']);
  $data = str_replace('data:image/png;base64,', '', $data);
  $data = str_replace(' ', '+', $data);
  $imageData = base64_decode($data);
  $filename = 'trees/' . $name . '-' . date("YmdHis") . '.png';
  $file = fopen($filename, 'wb');
  fwrite($file, $imageData);
  fclose($file);
  echo 'Image saved successfully as ' . $filename;
  // email
  $imageUrl = "https://nametree.oo8.be/" . $filename;
  $message = "<html><body><p>New nametree image:\r\n<a href='".$imageUrl."'>View image</a></p>\r\n";
  $message .= "<img style='width:600px; border:1px solid gray;' src='".$imageUrl."'>\r\n";
  $message .= "</body></html>";
  $headers[] = 'MIME-Version: 1.0';
  $headers[] = 'Content-type: text/html; charset=iso-8859-1';
  mail('gladyouask@proton.me', 'Nametree submission', $message, implode("\r\n", $headers));
} else {
  echo 'No image data received.';
}

function slugify($text, string $divider = '-'){
  $text = preg_replace('~[^\pL\d]+~u', $divider, $text);
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  $text = preg_replace('~[^-\w]+~', '', $text);
  $text = trim($text, $divider);
  $text = preg_replace('~-+~', $divider, $text);
  $text = strtolower($text);
  if (empty($text)) {
    return 'n-a';
  }
  return $text;
}
?>
