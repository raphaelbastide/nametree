let d = document
let b = d.body
let network = null;
let imagePath = "img/isabel.jpg"
let mainColor = getComputedStyle(b).getPropertyValue('--c1')
let whiteColor = getComputedStyle(b).getPropertyValue('--c2')
let lightColor = getComputedStyle(b).getPropertyValue('--cLight')
let commentColor = getComputedStyle(b).getPropertyValue('--cComments')
let stored = true
let initialMessage = "Double-click on names to edit it, or on empty spaces to add a new name"

message(initialMessage)

let nodes = new vis.DataSet([{
  id: 1,
  label: 'Isabel',
  shape:"image",
  image: imagePath,
  font:{color:mainColor,vadjust:-53, strokeColor:whiteColor, strokeWidth:10},
  x:20,
  y:-220,
  size: 160,
},
{id: 2,label: 'Lisabelle', x:-300,y:50},
{id: 3,label: 'Blizzard',x:-300,y:150},
{id: 4,label: 'Blizz',x:-280,y:250},
{id: 5,label: 'Pastaplate',x:310,y:35},
{id: 8,label: 'Fartplate',x:300,y:145},
{id: 9,label: 'Fartabelle',x:50,y:300},
{id: 10,label: 'Fluffabel',x:50,y:40},
{id: 11,label: 'Fluffbundle',x:50,y:150},
{id: 6,label: 'DROP A NEW IMAGE HERE',
  chosen:false,
  x:300,y:-250,
  font:{size:13, color: commentColor},
  color: {background: whiteColor, border:'transparent'}
},
{id: 7,size:2, shape: "circle",size:0,font:{ size:0,},color:whiteColor,x:215,y:-200,chosen:false}]);

let edges = new vis.DataSet([
{from: 1,to: 2},
{from: 2,to: 3},
{from: 3,to: 4},
{from: 1,to: 5},
{from: 5,to: 8},
{from: 8,to: 9},
{from: 2,to: 9},
{from: 1,to: 10},
{from: 10,to: 11},
{from: 6,to: 7,
  color: commentColor,
  arrows: {
    to: {
      enabled: true,
      type: "image",
      imageWidth: 14,
      imageHeight: 14,
      src: "img/arrow2.svg",
    },
  },
}]);

let data = {
  nodes: nodes,
  edges: edges
};

function draw() {
  let container = d.getElementById('mynetwork');
  let options = {
    nodes:{
      shape:"box",
      // chosen:false,
      labelHighlightBold: false,
      borderWidthSelected: 1,
      borderWidth:0,
      color: {
        background:"#e9e0ed",
        highlight: { background: whiteColor, border: mainColor },
      },
      font:{size:30,color:mainColor,vadjust:2, face:"hanken"},
    },
    physics:{
      enabled: false,
      hierarchicalRepulsion: {
        nodeDistance: 130,
      },
    },
    interaction:{
      dragView: false,
      zoomView: false,
    },
    edges:{
      // chosen: false,
      color: lightColor,
      smooth:{
        enabled:true,
        type:"dynamic",
        forceDirection: "none"
      },
      width: 3,
      arrows: {
        to: {
          enabled: true,
          type: "image",
          imageWidth: 24,
          imageHeight: 24,
          src: "img/arrow.svg",
        },
      },
    },
    manipulation: {
      addNode: function (data, callback) {
        d.getElementById('node-label').value = data.label;
        d.getElementById('saveButton').onclick = saveData.bind(this, data, callback);
        d.getElementById('cancelButton').onclick = clearPopUp.bind();
        d.getElementById('network-popUp').style.display = 'flex';
        d.getElementById('node-label').focus()
      },
      editNode: function (data, callback) {
        let id = data.id
        d.getElementById('node-label').value = data.label;
        d.getElementById('saveButton').onclick = saveData.bind(this, data, callback);
        d.getElementById('cancelButton').onclick = cancelEdit.bind(this, callback);
        d.getElementById('deleteButton').onclick = deleteNode.bind(this, id);
        d.getElementById('network-popUp').style.display = 'flex';
        d.getElementById('node-label').focus()
      },
      addEdge: function (data, callback) {
        b.classList.remove('linking')
        message(initialMessage)
        if (data.from == data.to) {
          let r = confirm("Do you want to connect the node to itself?");
          if (r == true) {
            callback(data);
          } 
        } else {
          callback(data);
        }
      }
    },
  };
  network = new vis.Network(container, data, options);
}

draw()


d.querySelector(".canvas-box").addEventListener("keydown", handleKeyDown);
function handleKeyDown(e) {
  if (e.code == "Delete") {
    network.deleteSelected()    
  }
}

b.addEventListener('dragover', handleDragOver);
b.addEventListener('dragenter', handleDragEnter);
b.addEventListener('drop', handleDrop);

d.querySelector('.delete').onclick = function(){
  network.deleteSelected()
}
d.querySelector('.rename').onclick = function(){
  network.editNode()
}
d.querySelector('.clear').onclick = function(){
  if (confirm("Are you sure to clear the diagram?")) {
    clear()
  }
}
d.querySelector('.pre-download').onclick = function(){
  preDownload()
}
d.querySelector('.store-download').onclick = function(){
  download()
  d.querySelector('.download-popup').style.display = "none"
}
d.querySelector('.cancel-download').onclick = function(){
  d.querySelector('.download-popup').style.display = "none"
}
d.querySelector('#stored').onclick = function(){
  stored = d.querySelector('#stored').checked
}
d.querySelector('.new-edge').onclick = function(){
  message('Click and drag from a name to another to create a link')
  network.manipulation.addEdgeMode()
  b.classList.add('linking')
}

function clearPopUp() {
  d.getElementById('cancelButton').onclick = null;
  d.getElementById('saveButton').onclick = null;
  d.getElementById('network-popUp').style.display = 'none';
}

function cancelEdit(callback) {
  clearPopUp();
  callback(null);
}

function saveData(data, callback) {
  data.label = d.getElementById('node-label').value
  clearPopUp();
  callback(data);
}

function deleteNode(data){
  network.selectNodes([data])
  network.deleteSelected()
  clearPopUp();
}

// Drop image

function handleDragOver(event) {event.preventDefault();}
function handleDragEnter(event) {event.preventDefault();}
function handleDrop(event) {
  event.preventDefault();
  const file = event.dataTransfer.files[0];
  if (file.type.startsWith('image/')) {
    const reader = new FileReader();
    reader.onload = function(e) {
      imagePath = e.target.result
      nodes.update({id: 1, image: imagePath});
      network.selectNodes([6,7])
      network.deleteSelected();
    };
    reader.readAsDataURL(file);
  } else {
    alert('Please drop an image file.');
  }
}
function uploadImage(imageData) {
  let xhr = new XMLHttpRequest()
  let mainName = node = nodes.get(1).label
  xhr.open('POST', 'save-image.php', true)
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4 && xhr.status === 200) {
      console.log(xhr.responseText)
    }
  };
  xhr.send('name=' + mainName + '&imageData=' + encodeURIComponent(imageData))
}

function addNode(params){
  if((params.nodes.length == 0) && (params.edges.length == 0)) {
    var updatedIds = nodes.add([{
      label:'name',
      x:params.pointer.canvas.x,
      y:params.pointer.canvas.y
    }]);
    network.selectNodes([updatedIds[0]]);
    network.editNode();
  }
}

function clear() {
  let sel = []
  nodes.forEach(node => {
    if (node.id != 1 && node.id != 6 && node.id != 7) {
      sel.push(node.id)
    }
  });
  network.selectNodes(sel)
  network.deleteSelected()
}

function message(m){
  d.querySelector('.message').innerHTML = m
}

// Save, download

function download(){
  // Unselect possible selected nodes
  network.unselectAll()
  // Remove unwanted nodes (comments)
  let sel = []
  nodes.forEach(node => {
    if (node.id == 6 || node.id == 7) {
      sel.push(node.id)
    }
  });
  network.selectNodes(sel)
  network.deleteSelected()
  // Save canvas
  let link = document.createElement('a')
  let mainName = node = nodes.get(1).label
  link.download = `${mainName}.png`
  let imgData = document.querySelector('canvas').toDataURL()
  link.href = imgData
  if (stored) {
    uploadImage(imgData)
  }
  link.click();
}

function preDownload(){
  d.querySelector('.download-popup').style.display = 'flex';
}


// Network events

network.on("doubleClick", function (params) {
  network.editNode()
  addNode(params)
});

network.on("beforeDrawing", function(ctx) {		
  canvas = network.canvas.frame.canvas;
	ctx = canvas.getContext('2d')
  ctx.fillStyle = whiteColor;
  ctx.fillRect(-1000, -1000, 10000, 10000);
  ctx.drawImage(document.getElementById("logo"), -390, -430);
});

network.on("select", function(params){
  if (params.edges.length > 0 || params.nodes.length > 0) {    
    b.querySelectorAll('.for-selection').forEach(el => {
      el.removeAttribute('disabled')
    });
  }else{
    b.querySelectorAll('.for-selection').forEach(el => {
      el.setAttribute('disabled','true')
    });
  }
})
